
class kuePesanan extends kue{
    public kuePesanan(String name, double price,double berat) {
        super(name, price);
        setBerat(berat);
        
    }
    private double berat;
    private double getBerat(){
        return berat;
    }
    private void setBerat(double berat){
        this.berat = berat;
    }
    public double hitungHarga(){
        return super.getPrice()*berat;
    }
    @Override
    public double Berat(){
        return berat;
    }
    @Override
    public double getPrice(){
        return super.getPrice();
    }
    @Override
    public void setPrice(double price){
        super.setPrice(price);
    }
    @Override
    public double Jumlah() {
        return 0;
    }
}
